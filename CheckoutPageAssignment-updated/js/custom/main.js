$(function() {
  // Grab the template script
  var theTemplateScript = $("#cart-template").html();

  // Compile the template
  var theTemplate = Handlebars.compile(theTemplateScript);

  // Define our data object (use local server to access)
  // $.getJSON("js/custom/data/data.json", function(result){
  //     var context = result;
  // });
  //Alternatively refer this data
  var context = {
    cartItems: [
      {
        img: "img/Capture.JPG",
        name: "SOLID GREEN COTON TSHIRT",
        style: "MS13KT1906",
        color: "Green",
        size: "S",
        qty: 1,
        price: 120
      },
      {
        img: "img/pink.png",
        name: "PINK RAINBOW PRINT TSHIRT",
        style: "MS13KT1907",
        color: "Red",
        size: "B",
        qty: 1,
        price: 90
      },
      {
        img: "img/white.png",
        name: "BLUE FLOWER PATTERN SHIRT",
        style: "MS13KT1908",
        color: "Yellow",
        size: "L",
        qty: 1,
        price: 110
      }
    ],
    heading: "your shopping bag"
  };

  // Pass our data to the template
  var theCompiledHtml = theTemplate(context);

  // Add the compiled html to the page
  $(".content-placeholder").html(theCompiledHtml);

  var lengthOfCart = context.cartItems.length;
  document.getElementById("items-count").innerHTML = lengthOfCart;
  document.getElementsByClassName("items-count").innerHTML = lengthOfCart;
});

function myFunction1(index) {
  var currPqtyId = "pqty-" + index;
  var productPrice = document.getElementById("product-price-" + index).value;

  var productQuantity = document.getElementById(currPqtyId).value;
  document.getElementById("update-qty-" + index).innerHTML = productQuantity;
  document.getElementById("update-price-" + index).innerHTML =
    productQuantity * productPrice;
  myFunction2();
}
function myFunction2() {
  var total = 0;
  var count = document.getElementById("items-count").innerText;

  for (i = 0; i < count; i++) {
    var pPrice = document.getElementById("update-price-" + i).innerText;
    total = total + Number(pPrice);
  }

  var subtotal = (document.getElementById("sub-total").innerHTML = total);
  var discount = document.getElementById("promo-code-discount").innerText;
  var checkoutTotal = (document.getElementById("checkout-total").innerHTML =
    subtotal - discount);
}

function mobilemyFunction(index) {
  var mcurrPqtyId = "mobile-pqty-" + index;
  var mproductPrice = document.getElementById("mobile-product-price-" + index)
    .value;
  var mproductQuantity = document.getElementById(mcurrPqtyId).value;

  var productquantity = document.getElementById("mobile-pqty-" + index).value;
  if (productquantity >= 0) {
    document.getElementById("hide-validate").style.visibility = "visible";
    document.getElementById("mobile-productPrice-" + index).style.color =
      "grey";

    document.getElementById("mobile-productPrice-" + index).innerHTML =
      mproductQuantity * mproductPrice;

    var count = document.getElementById("items-count").innerText;

    var mtotal = 0;
    for (j = 0; j < count; j++) {
      var mPrice = document.getElementById("mobile-productPrice-" + j)
        .innerText;

      mtotal = mtotal + Number(mPrice);
    }
    var msubtotal = (document.getElementById("sub-total").innerHTML = mtotal);
    var mdiscount = document.getElementById("promo-code-discount").innerText;
    var mcheckoutTotal = (document.getElementById("checkout-total").innerHTML =
      msubtotal - mdiscount);
  } else {
    document.getElementById("mobile-productPrice-" + index).innerHTML =
      "Incorrect Value";

    document.getElementById("hide-validate").style.visibility = "hidden";
    document.getElementById("mobile-productPrice-" + index).style.color = "red";

    var count = document.getElementById("items-count").innerText;

    var mtotal = 0;
    for (j = 1; j < count; j++) {
      var mPrice = document.getElementById("mobile-productPrice-" + j)
        .innerText;

      mtotal = mtotal + Number(mPrice);
    }
    var msubtotal = (document.getElementById("sub-total").innerHTML = mtotal);
    var mdiscount = document.getElementById("promo-code-discount").innerText;
    var mcheckoutTotal = (document.getElementById("checkout-total").innerHTML =
      msubtotal - mdiscount);
  }
}

function popFunction(index2) {
  $("#popup1").show();
  var title = document.getElementById("productTitle-" + index2).innerText;
  var price = document.getElementById("product-price-" + index2).value;
  var img = document.getElementById("img-src-" + index2).value;
  document.getElementById("pop-pname").innerHTML = title;
  document.getElementById("pop-price").innerHTML = "$" + price;
  document.getElementById("img-div").innerHTML = "<img src=" + img + " />";
  document.getElementById("pop-index").value = index2;
}
function editItem() {
  var index3 = document.getElementById("pop-index").value;
  var selctdIndex = document.getElementById("drop-select1").selectedIndex;
  var selctdvalue = document.getElementById("drop-select1").options[selctdIndex]
    .text;
  document.getElementById("product-size-" + index3).innerHTML = selctdvalue;
  document.getElementById(
    "mobile-productSize-" + index3
  ).innerHTML = selctdvalue;
  $("#popup1").hide();
}
